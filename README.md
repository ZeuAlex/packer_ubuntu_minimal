Packer - Ubuntu Minimal
=======================

Packer repository to build an **Ubuntu minimal vagrant box**.

Releases are available on [Vagrant Cloud](https://app.vagrantup.com/ZeuAlex/boxes/ubuntu_minimal).


###### Ubuntu version :
18.04 (Bionic Beaver)

###### Compatibility :
- Virtualbox


Requirements
------------

- [Virtualbox](https://www.virtualbox.org/)
- [Packer](https://www.packer.io/)
- [Vagrant](https://www.vagrantup.com/)


Build
-----

To build the box, remove vagrant-cloud post-processor from template and run :

```bash
# From project root
packer build -var 'version=0.0' ubuntu_minimal.json
```
Box will be placed in `builds` repository.


Debug
-----

To enable Packer verbosity, build with command :

```bash
PACKER_LOG=1 packer build -var 'version=0.0' ubuntu_minimal.json
```

To see build stages on GUI, set in template :

```yaml
"headless": false
```

You can also check hardware parameters in your VM Manager during build.


Testing
-------

###### Using cloud pre-built box

```bash
vagrant init ZeuAlex/ubuntu_minimal
```


###### Using locally built box

You can use included Vagranfile :

```bash
# From project root
vagrant up
vagrant ssh
```

**Be careful that Vagrant doesn't use an outdated box !!!**

Because a locally built box does not have a versioning mechanism, Vagrant cannot see than the box has been rebuilt.

If you rebuild a box that has already been mounted locally, you have to remove previous box before executing `vagrant up` command.
Like this, Vagrant will reimport the box.

```bash
vagrant box list
vagrant box remove <box>
```