#!/bin/bash


# Delete firmware packages
rm -rf /lib/firmware/*
rm -rf /usr/share/doc/linux-firmware/*

# Delete orphan packages and cache
apt-get -y autoremove --purge
apt-get -y clean